package com.devcamp.s50.rectangle_restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RectangleRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RectangleRestapiApplication.class, args);
	}

}
