package com.devcamp.s50.rectangle_restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleControl {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getArea(){
        float lengthh = 6;
        float widthh = 5;
        Rectangle rectangle = new Rectangle(lengthh, widthh);
        return rectangle.getArea();
    }

    @CrossOrigin
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(){
        float lengthh = 6;
        float widthh = 5;
        Rectangle rectangle = new Rectangle(lengthh, widthh);

        return rectangle.getPerimeter();
    }
}
